from services.cron import CronTask
import datetime
from .cron.supervisor_task import supervisor_task_function


class SupervisorTask(CronTask):
    """
    Task which every 10 minutes looks for current processes and
    set "running_now" to False, if it too much time in processed.
    """
    RUN_EVERY_MINS = 10
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        supervisor_task_function()