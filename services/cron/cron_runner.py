# -*- coding: utf-8 -*-

import os
from typing import List
from . import CronTask


class CronRunner:

    def __init__(self, base_dir):
        self.base_dir = base_dir

    def run(self):
        crons_list = self._get_crons(self.base_dir)
        for cron in crons_list:
            cron()

    def list(self):
        crons_list = self._get_crons(self.base_dir)
        for cron in crons_list:
            print(cron.__name__)

    def _find_crontasks(self, _task) -> List[CronTask]:
        lst = []
        for key in _task.__dict__:
            task_cls = eval("_task." + key)
            if "CronTask" in str(task_cls.__repr__) and "my_task" in str(task_cls):
                lst.append(task_cls)
        return lst

    def _get_crons(self, base_dir):
        dirs = os.listdir(base_dir)
        crons_list = []
        for dir in dirs:
            try:
                _task = __import__(dir + ".my_task", globals(), locals(), ["*"])
                crons = self._find_crontasks(_task)
                crons_list += crons
            except:
                continue
        return crons_list