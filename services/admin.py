from django.contrib import admin
from services.submodels.task import Task, TaskEvent
from services.submodels.consumer.model import Consumer
from services.submodels.task.scheduller_model import Scheduller


class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'run_every_mins',  'run_at_time', 'running_now', 'alert', 'include', )
    filter_horizontal = ('consumers',)
    search_fields = (
        "name",
    )
admin.site.register(Task, TaskAdmin)


class TaskEventAdmin(admin.ModelAdmin):
    list_display = ('task', 'started',  'finished', 'success', )
    list_display_links = ('task', )
    search_fields = (
        "task__name",
    )
admin.site.register(TaskEvent, TaskEventAdmin)


class ConsumerAdmin(admin.ModelAdmin):
    search_fields = (
        "name",
    )
admin.site.register(Consumer, ConsumerAdmin)


class SchedullerAdmin(admin.ModelAdmin):
    fields = (("name"),
              ("monday", "monday_from", "monday_to"),
              ("tuesday", "tuesday_from", "tuesday_to"),
              ("wednesday", "wednesday_from", "wednesday_to"),
              ("thursday", "thursday_from", "thursday_to"),
              ("friday", "friday_from", "friday_to"),
              ("saturday", "saturday_from", "saturday_to"),
              ("sunday", "sunday_from", "sunday_to"),)

admin.site.register(Scheduller, SchedullerAdmin)
