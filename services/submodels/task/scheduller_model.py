from django.db import models
from django.utils import timezone


class Scheduller(models.Model):
    name = models.CharField(max_length=1000)

    monday = models.BooleanField(default=True)
    monday_from = models.TimeField(default=None, null=True, blank=True)
    monday_to = models.TimeField(default=None, null=True, blank=True)

    tuesday = models.BooleanField(default=True)
    tuesday_from = models.TimeField(default=None, null=True, blank=True)
    tuesday_to = models.TimeField(default=None, null=True, blank=True)

    wednesday = models.BooleanField(default=True)
    wednesday_from = models.TimeField(default=None, null=True, blank=True)
    wednesday_to = models.TimeField(default=None, null=True, blank=True)

    thursday = models.BooleanField(default=True)
    thursday_from = models.TimeField(default=None, null=True, blank=True)
    thursday_to = models.TimeField(default=None, null=True, blank=True)

    friday = models.BooleanField(default=True)
    friday_from = models.TimeField(default=None, null=True, blank=True)
    friday_to = models.TimeField(default=None, null=True, blank=True)

    saturday = models.BooleanField(default=True)
    saturday_from = models.TimeField(default=None, null=True, blank=True)
    saturday_to = models.TimeField(default=None, null=True, blank=True)

    sunday = models.BooleanField(default=True)
    sunday_from = models.TimeField(default=None, null=True, blank=True)
    sunday_to = models.TimeField(default=None, null=True, blank=True)

    def __str__(self):
        return self.name

    def is_time(self):
        now = timezone.localtime(timezone.now())
        time = now.time()
        weekday_number = now.weekday() + 1
        dct = self.__mapper[str(weekday_number)]
        if not dct["bool"]:
            return False

        time_from = dct["from"]
        time_to = dct["to"]
        if time_from is None and time_to is None:
            return True
        if time_from is None:
            if time >= time_from:
                return True
        if time_to is None:
            if time <= time_to:
                return True
        return False


    @property
    def __mapper(self):
        mapper = {
            "1": {
                "bool": self.monday,
                "from": self.monday_from,
                "to": self.monday_to
            },
            "2": {
                "bool": self.tuesday,
                "from": self.tuesday_from,
                "to": self.tuesday_to
            },
            "3": {
                "bool": self.wednesday,
                "from": self.wednesday_from,
                "to": self.wednesday_to
            },
            "4": {
                "bool": self.thursday,
                "from": self.thursday_from,
                "to": self.thursday_to
            },
            "5": {
                "bool": self.friday,
                "from": self.friday_from,
                "to": self.friday_to
            },
            "6": {
                "bool": self.saturday,
                "from": self.saturday_from,
                "to": self.saturday_to
            },
            "7": {
                "bool": self.sunday,
                "from": self.sunday_from,
                "to": self.sunday_to
            },
        }
        return mapper

